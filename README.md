# README

## Dependencies

- Node ~> 12.16.1
- NPM ~> 6.13.4

### Install Node dependencies

    npm install

## Setup the environment

Tests can be run against localhost or a specific environment, to make the required setup create a file `cypress.env.json`
in the root of the project and copy the content of `cypress.env.json.sample`. Make sure to update the information according
to your user's credentials.

For this demo's purpose, default admin credentials are the following (file has been already added)

    {
      "host": "localhost:3000",
      "userEmail": "admin@example.com",
      "userPassword": "test123"
    }

## Run the target platform (Solidus E-commerce) via docker

    docker run --rm -it -p 3000:3000 solidusio/solidus-demo:latest

## Open Test Runner

To run the interactive Cypress test runner

    npx cypress open

That will open a window in which all tests are listed. You can run all tests or run a specific test by clicking it

## Run tests on headless mode

To run the tests without opening Cypress UI

    npx cypress run

To run a specific test

    npx cypress run --spec cypress/automation/features/storefront/{testFileName}.js

To override env variables

    npx cypress run --env host=hostUrl,userEmail=youremail@email.com,password=yourpassword

## How are tests organized?

Tests follow the Page Object pattern, so in order to interact with the UI you have to create a page or add the required locators
to existent pages. Pages can be found on `cypress/automation/pages`. Example page:

    class PagePage {
      // Add page locators here
      constructor() {
        this.submitButton = '#submit';
        this.menuItem = '.menu-item';
        this.successMessage = '.flash-success';
      }

      // Actions on locators
      submitForm() {
        cy.get(this.submitButton).click();

        return this;
      }
    }

Feature specs can be found on `cypress/automation/features` and they make use of pages to interact with the UI. Example spec:

    import samplePage from '../../pages/storeFrontPages';

    const samplePage = new SamplePage();

    scenario('Example Spec', () => {
      it('should submit a form', () => {
        visit('/some_path');

        samplePage.fillInAddress().fillInPayment().submitForm();

        # Example of assertion
        cy.get(this.successMessage).should('be.present');
      })
    })
