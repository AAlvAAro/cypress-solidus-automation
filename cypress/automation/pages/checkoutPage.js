class CheckoutPage {
  constructor() {
    this.addToCartButton = '#add-to-cart-button';
    this.couponCode = 'form > #coupon_code';
    this.quantity = '#order_line_items_attributes_0_quantity';
    this.checkoutLink = '#checkout-link';
    this.firstName = '#order_bill_address_attributes_firstname';
    this.lastName = '#order_bill_address_attributes_lastname';
    this.addressOne = '#order_bill_address_attributes_address1';
    this.addressTwo = '#order_bill_address_attributes_address2';
    this.city = '#order_bill_address_attributes_city';
    this.country = '#order_bill_address_attributes_country_id';
    this.state = '#order_bill_address_attributes_state_id';
    this.zipCode = '#order_bill_address_attributes_zipcode';
    this.phone = '#order_bill_address_attributes_phone';
    this.continueButton = '.continue';
    this.submitAction = 'div > form > [type="submit"]';

    this.cardNumber = '#card_number';
    this.cardExpiry = '#card_expiry';
    this.cardCode = '#card_code';
    this.submitButton = 'input[name="commit"]';
  }

  addAddress() {
    cy.get(this.firstName).clear().type('John');
    cy.get(this.lastName).clear().type('Smith');
    cy.get(this.addressOne).clear().type('First 38');
    cy.get(this.addressTwo).clear().type('Int 3');
    cy.get(this.city).clear().type('Florence');
    cy.get(this.country).select('United States of America');
    cy.get(this.state).select('Alabama');
    cy.get(this.zipCode).clear().type('35630');
    cy.get(this.phone).clear().type('6623121212');
    cy.get(this.continueButton).click();
    cy.get(this.continueButton).click();

    return this;
  }

  addCouponCode(code) {
    cy.get(this.addToCartButton).click();
    cy.visitPath('/cart');
    cy.get(this.couponCode).type(code);
    cy.get(this.submitAction).click();

    return this;
  }

  chooseQuantity(number) {
    cy.get(this.quantity).clear().type(number);
    cy.get(this.checkoutLink).click();

    return this;
  }

  addPayment() {
    cy.get(this.cardNumber).type('4111111111111111');
    cy.get(this.cardExpiry).type('10/27');
    cy.get(this.cardCode).type('123');

    return this;
  }

  submit() {
    cy.get(this.submitButton).click();
    cy.get(this.submitButton).click();
  }
}

export default CheckoutPage;
