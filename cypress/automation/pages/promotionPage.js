class PromotionPage {
  constructor() {
    this.promotionName = '#promotion_name';
    this.promotionLimit = '#promotion_usage_limit';
    this.promotionCode = '#single_code';
    this.createPromotionButton = '.btn';
    this.discountRules = '#promotion_rule_type';
    this.addDiscountRules = '#new_product_rule_form > fieldset > .filter-actions > .btn';
    this.productGreaterThan = 'input[class="form-control number-with-currency-amount"]';
    this.addProductGreaterThan = '.no-border-top > .promotion-update > .btn';
    this.chooseAction = '#action_type';
    this.addAction = '#new_promotion_action_form > fieldset > .filter-actions > .btn';
    this.flatPercent = 'input[class= "input_decimal fullwidth"]';
    this.addFlatPercent = '#action_fields > #edit_promotion_1 > .filter-actions > .btn';
    this.removeAction = 'a[data-action="remove"]';
  }

  createPromotion(opts = {}) {
    let promotionInfo = {
      promotionName: opts['promotionName'] || 'Fifty percent discount',
      promotionLimit: opts['promotionLimit'] || '1',
      promotionCode: opts['promotionCode'] || '50OFF',
      discountRules: opts['discountRules'] || 'Item Total',
      productGreaterThan: opts['productGreaterThan'] || '19',
      chooseAction: opts['chooseAction'] || 'Create whole-order adjustment',
      flatPercent: opts['flatPercent'] || '50',
    };

    cy.get(this.promotionName).type(promotionInfo['promotionName']);
    cy.get(this.promotionLimit).type(promotionInfo['promotionLimit']);
    cy.get(this.promotionCode).type(promotionInfo['promotionCode']);
    cy.get(this.createPromotionButton).click();
    cy.get(this.discountRules).select(promotionInfo['discountRules']);
    cy.get(this.addDiscountRules).click();
    cy.get(this.productGreaterThan).first().clear().type(promotionInfo['productGreaterThan']);
    cy.get(this.addProductGreaterThan).click();
    cy.get(this.chooseAction).select(promotionInfo['chooseAction']);
    cy.get(this.addAction).click();
    cy.get(this.flatPercent).first().clear().type(promotionInfo['flatPercent']);
    cy.get(this.addFlatPercent).click();

    return this;
  }

  deletePromoCode() {
    cy.get(this.removeAction).click();

    return this;
  }
}

export default PromotionPage;
