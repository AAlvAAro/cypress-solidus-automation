import PromotionPage from '../../pages/promotionPage';
import CheckoutPage from '../../pages/checkoutPage';

const promotionPage = new PromotionPage();
const checkoutPage = new CheckoutPage();

describe('Promotion', () => {
  beforeEach(() => {
    cy.loginUser();
  });

  context('when the promo code does not exist', () => {
    it('shows an error message', () => {
      cy.visitPath('/products/ruby-baseball-jersey');
      checkoutPage.addCouponCode('40OFF');

      cy.contains("The coupon code you entered doesn't exist. Please try again.").should(
        'be.visible'
      );

      cy.logoutUser();
    });
  });

  context('when the promo code exists', () => {
    it('can finish a purchase with discount', () => {
      cy.visitPath('/admin/promotions/new');
      promotionPage.createPromotion();

      cy.visitPath('/products/ruby-baseball-jersey');
      checkoutPage.addCouponCode('50OFF').chooseQuantity(1).addAddress().addPayment().submit();

      cy.contains('Your order has been processed successfully');

      cy.visitPath('/admin/promotions');
      promotionPage.deletePromoCode();

      cy.logoutUser();
    });
  });
});
